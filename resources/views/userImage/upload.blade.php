@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')

                            <div class="col-md-8">

                                <div class="form-group row">

                                    <div class="col-md-12 text-md-center">
                                        <div class="alert" id="message" style="display: none"></div>

                                        <img id="uploaded_image" src="{{ $user->profile_image }}" alt="Profile image"
                                             class="profile-image">

                                        <table class="table table-responsive-sm">
                                            <tr>
                                                <form id="upload_form" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <p>{{ __('Select image to upload:') }}</p>
                                                    <td><input type="file" name="image" id="image"></td>
                                                    <td><input type="submit" name="upload" id="upload"
                                                               class="btn btn-primary" value="{{ __('Upload') }}"></td>
                                                </form>
                                                <form id="delete_form" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <td><input type="submit" name="delete" id="delete"
                                                               class="btn btn-danger" value="{{ __('Delete') }}"></td>
                                                </form>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><span class="text-muted">jpg, jpeg</span></td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/image.js') }}"></script>
@endpush

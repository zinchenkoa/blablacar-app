@extends('layouts.app')

@section('content')

    <header class="masthead text-center text-white">
        <div class="masthead-content">
            <div class="container">
                <a href="/login" class="btn btn-light btn-xl mt-5">{{ __('welcome.Login and find a ride') }}</a>
            </div>
        </div>
    </header>

    <section class="py-5">
        <div class="container">
            <h1>{{ __('welcome.New trips every day.') }}</h1>
            <br>

            <div class="row text-center">
                <div class="col-md-3 mb-3">
                    <div class="card h-100">
                        <div class="counter">
                            <h2 class="timer count-title count-number" data-to="{{ $stats->getTravelsToday() }}"
                                data-speed="1500"></h2>
                            <p class="count-text ">{{ __('welcome.Today') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="card h-100">
                        <div class="counter">
                            <h2 class="timer count-title count-number" data-to="{{ $stats->getTravelsYesterday() }}"
                                data-speed="1500"></h2>
                            <p class="count-text ">{{ __('welcome.Yesterday') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="card h-100">
                        <div class="counter">
                            <h2 class="timer count-title count-number" data-to="{{ $stats->getTravelsLastWeek() }}"
                                data-speed="1500"></h2>
                            <p class="count-text ">{{ __('welcome.Last week') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 mb-3">
                    <div class="card h-100">
                        <div class="counter">
                            <h2 class="timer count-title count-number" data-to="{{ $stats->getTravelsLastMonth() }}"
                                data-speed="1500"></h2>
                            <p class="count-text ">{{ __('welcome.Last month') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section>
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-6">
                    <img class="img-fluid"
                         src="https://d1ovtcjitiy70m.cloudfront.net/vi-1/images/rebranding/homeblock_driver_desktop.jpg"
                         alt="">
                </div>
                <div class="col-md-6">
                    <div class="p-5">
                        <h2>{{ __('welcome.Where do you want to drive to?') }}</h2>
                        <p>{{ __('welcome.Make this your least expensive journey ever.') }}</p>
                        <a href="/register"
                           class="btn btn-dark btn-xl mt-5">{{ __('welcome.No account? Register!') }}</a>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="py-5">
        <div class="container">


            <h1>{{ __('welcome.Go literally anywhere.') }}</h1>
            <br>

            <div class="row">
                <div class="col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title">{{ __('welcome.Smart') }}</h2>
                            <p class="card-text">{{ __('welcome.With access to millions of journeys, you can quickly find people nearby travelling your way.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title">{{ __('welcome.Simple') }}</h2>
                            <p class="card-text">{{ __('welcome.Select who you want to travel with.') }}</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mb-4">
                    <div class="card h-100">
                        <div class="card-body">
                            <h2 class="card-title">{{ __('welcome.Seamless') }}</h2>
                            <p class="card-text">{{ __('welcome.Get to your exact destination, without the hassle. Carpooling cuts out transfers, queues and the waiting around the station time.') }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection

@push('scripts')
    <script type="text/javascript" src="{{ asset('js/counter.js') }}"></script>
@endpush

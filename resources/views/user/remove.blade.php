@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')
                            <div class="col-8">

                                @if(Session::has('message'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                @endif
                                <p>
                                    {{ __('If you\'d like to close your account because you weren\'t able to solve a problem, please take a look at our tips first:') }}
                                </p>

                                <ul>
                                    <li>{{ __('To change your phone number or your email address, you can ') }}
                                        <a
                                                href="/user/edit">{{ __('edit your profile') }}</a>.
                                    </li>
                                    <li>{{ __('To resolve a technical issue, please, contact us') }}</a>.
                                    </li>
                                    <li>{{ __('To deal with a rating that you feel is unjustified, please, contact us') }}</a>.
                                    </li>
                                </ul>

                                <form action="/user/{{ $user->id }}" method="post">
                                    @csrf
                                    @method('DELETE')
                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-danger"
                                                    onclick="return confirm('{{ __('Are you sure you want to delete your profile?') }}')">
                                                {{ __('Delete profile') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

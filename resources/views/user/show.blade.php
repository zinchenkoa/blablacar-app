@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

                        <div class="offset-md-3 col-md-6">

                            @if (Session::has('success'))
                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                            @endif
                            @if (Session::has('failure'))
                                <div class="alert alert-danger">{{ Session::get('failure') }}</div>
                            @endif

                            <div class="text-md-center">
                                <img src="{{ $user->profile_image }}" alt="Profile image" class="profile-image">
                                <h4>{{ $user->full_name }}</h4>
                                @if(isset($user->birth_year))
                                    <p>{{ $user->age }} {{ __('y.o.') }}</p>
                                @endif
                                @if(isset($user->about))
                                    <p>{{ $user->about }}</p>
                                @endif
                            </div>
                            <hr>
                            <p>Email: {{ $user->email }}</p>
                            <hr>
                            <p class="small margin-0">{{ __('Offered trips:') }} {{ $countUserTravels }}</p>
                            <p class="small"> {{ __('Registered at:') }} {{ $user->register_month }}</p>
                            <hr>
                            @if(!$reviews->isEmpty())
                                @if(isset($user->avg_score))
                                    <h5>{{ __('Rating:') }} {{ $user->avg_score }}/5 -
                                        {{ $reviewsCount }} {{ __('feedback') }}
                                    </h5>
                                @endif

                                @foreach($reviews as $review)
                                    <hr>
                                    <div class="row">
                                        <div class="col-xs-12 col-sm-12 col-md-2 col-lg-3 offset-lg-3 offset-md-4">
                                            <img src="{{ $review->owner->profile_image }}"
                                                 alt="Profile image" class="profile-image-small">
                                        </div>
                                        <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 text-md-left text-lg-left">
                                            <p><b>{{ $review->literal_score }}</b></p>
                                            <p><b>From
                                                    <a href="/user/{{ $review->owner->id }}">
                                                        {{ $review->owner->first_name }}
                                                    </a>
                                                </b>: {{ $review->about }}
                                            </p>
                                            <p class="small">{{ $review->literal_created_at }}</p>
                                        </div>
                                    </div>
                                @endforeach
                                {{ $reviews->links() }}

                            @else
                                <p>{{ __('No feedback for') }} {{ $user->first_name }}</p>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection

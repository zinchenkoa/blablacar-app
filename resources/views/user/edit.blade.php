@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')
                            <div class="col-md-8">

                                @if(Session::has('message'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                @endif

                                <form action="/user/{{ $user->id }}" method="post">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group row">
                                        <label for="first_name" class="col-sm-4 col-form-label text-md-right">{{ __('First name') }}</label>

                                        <div class="col-md-6">
                                            <input id="first_name" type="text"
                                                   class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}"
                                                   name="first_name"
                                                   value="{{ old('first_name', $user->first_name) }}">
                                            @if ($errors && $errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label for="last_name" class="col-sm-4 col-form-label text-md-right">{{ __('Last name') }}</label>

                                        <div class="col-md-6">
                                            <input id="last_name" type="text"
                                                   class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}"
                                                   name="last_name" value="{{ old('last_name', $user->last_name) }}">
                                            @if ($errors && $errors->has('last_name'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                            @endif
                                        </div>

                                    </div>

                                    <div class="form-group row">
                                        <label for="email" class="col-md-4 col-form-label text-md-right">Email</label>

                                        <div class="col-md-6">
                                            <input id="email" type="email"
                                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                                   name="email" value="{{ old('email', $user->email) }}">
                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="phone" class="col-md-4 col-form-label text-md-right">{{ __('Phone') }}</label>

                                        <div class="col-md-6">
                                            <input id="phone" type="tel"
                                                   class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                                   name="phone" value="{{ old('phone', $user->phone) }}">
                                            @if ($errors->has('phone'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="birth_year" class="col-md-4 col-form-label text-md-right">{{ __('Year of birth') }}</label>

                                        <div class="col-md-6">
                                            <input id="birth_year" type="year"
                                                   class="form-control{{ $errors->has('birth_year') ? ' is-invalid' : '' }}"
                                                   name="birth_year"
                                                   value="{{ old('birth_year', $user->birth_year) }}">
                                            @if ($errors->has('birth_year'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('birth_year') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row">
                                        <label for="about" class="col-md-4 col-form-label text-md-right">{{ __('About me') }}</label>

                                        <div class="col-md-6">
                                    <textarea id="about" name="about" rows="5" placeholder="{{ __('Tell us about yourself.') }}"
                                              class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}">{{ old('about', $user->about) }}</textarea>
                                            @if ($errors->has('about'))
                                                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Save') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

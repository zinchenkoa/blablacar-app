@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')
                            <div class="col-8">

                                @if (Session::has('success'))
                                    <div class="alert alert-success">{{ Session::get('success') }}</div>
                                @endif
                                @if (Session::has('failure'))
                                    <div class="alert alert-danger">{{ Session::get('failure') }}</div>
                                @endif
                                <form action="{{ route('password.update') }}" method="post">
                                    @csrf
                                    @method('PUT')

                                    <div class="form-group{{ $errors->has('old') ? ' has-error' : '' }} row">
                                        <label for="old" class="col-sm-4 col-form-label text-md-right">{{ __('Old Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="old" type="password"
                                                   class="form-control{{ $errors->has('old') ? ' is-invalid' : '' }}"
                                                   name="old">
                                            @if ($errors->has('old'))
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $errors->first('old') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }} row">
                                        <label for="password"
                                               class="col-sm-4 col-form-label text-md-right">{{ __('New Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password" type="password"
                                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                                   name="password">
                                            @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : '' }} row">
                                        <label for="password-confirm" class="col-sm-4 col-form-label text-md-right">{{ __('Confirm New Password') }}</label>

                                        <div class="col-md-6">
                                            <input id="password-confirm" type="password"
                                                   class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                                   name="password_confirmation">
                                            @if ($errors->has('password_confirmation'))
                                                <span class="invalid-feedback" role="alert">
                                                     <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group row mb-0">
                                        <div class="col-md-8 offset-md-4">
                                            <button type="submit" class="btn btn-primary">
                                                {{ __('Submit') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
@endsection

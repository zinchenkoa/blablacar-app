@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card px-md-3">
                    <div class="card-body">
                        <h3>{{ __('Find a ride') }} </h3>
                        <form action="travel" method="get" id="form">

                            <div class="row">
                                <div class="col-md-4 mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-map-marker-alt"></i></div>
                                        </div>
                                        <input id="start" type="text" class="form-control" name="start"
                                               placeholder="{{ __('From') }}" required autofocus>
                                        <input id="start_id" hidden="hidden" type="text" name="start_id">
                                    </div>
                                    <div class="text-md-center">
                                        <a href="#" id="s_kiev" class="m-2">Kiev
                                        </a>
                                        <a href="#" id="s_odesa" class="m-2">Odesa
                                        </a>
                                        <a href="#" id="s_dnipro" class="m-2">Dnipro
                                        </a>
                                        <a href="#" id="s_lviv" class="m-2">Lviv
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-4 mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="fas fa-map-marker-alt"></i></div>
                                        </div>
                                        <input id="destination" type="text" class="form-control"
                                               name="destination"
                                               placeholder="{{ __('To') }}" required>
                                        <input id="destination_id" hidden="hidden" type="text"
                                               name="destination_id">
                                    </div>
                                    <div class="text-md-center">
                                        <a href="#" id="d_kiev" class="m-2">Kiev
                                        </a>
                                        <a href="#" id="d_odesa" class="m-2">Odesa
                                        </a>
                                        <a href="#" id="d_dnipro" class="m-2">Dnipro
                                        </a>
                                        <a href="#" id="d_lviv" class="m-2">Lviv</a>
                                    </div>
                                </div>
                                <div class="col-md-3 mb-3">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <div class="input-group-text"><i class="far fa-calendar-alt"></i></div>
                                        </div>
                                        <input id="date" type="date" min="{{ $today }}" class="form-control" name="date"
                                               required>
                                    </div>
                                    <div class="text-md-center">
                                        <a href="#" class="m-2"
                                           onclick="$('#date').val('{{ \Carbon\Carbon::now()->toDateString() }}'); return false;">{{ __('Today') }}
                                        </a>
                                        <a href="#" class="m-2"
                                           onclick="$('#date').val('{{ \Carbon\Carbon::tomorrow()->toDateString() }}'); return false;">{{ __('Tomorrow') }}
                                        </a>
                                    </div>
                                </div>
                                <div class="col-md-1 mb-3">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Find') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <div class="alert alert-light text-center" role="alert">
                            {{ __('The site works in demo mode. To get search results choose cities and date from the list below the input.') }}
                        </div>
                        <div class="text-md-center">
                            <img src="https://d1ovtcjitiy70m.cloudfront.net/vi-1/images/rebranding/illustrations/search.svg"
                                 width="640" height="419" alt="" aria-hidden="true" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/autocomplete.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/demo.js') }}"></script>
@endpush


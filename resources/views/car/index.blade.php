@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')
                            <div class="col-8">

                                @if(Session::has('message'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                @endif
                                <h4>{{ __('My car') }}:</h4>

                                @if($car)
                                    <hr>
                                    <p>{{ __('Brand') }}: <b>{{ $car->brand }}</b></p>
                                    <p>{{ __('Model') }}: <b>{{ $car->model }}</b></p>

                                    <form action="/user/car" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button type="submit" class="btn btn-danger"
                                                onclick="return confirm('{{ __('Are you sure you want to delete your car?') }}')">
                                            {{ __('Delete car') }}
                                        </button>
                                    </form>

                                @else
                                    <p>{{ __('What\'s your car brand?') }}</p>
                                    <form action="/user/car/" method="post">
                                        @csrf

                                        <div class="form-group row">
                                            <label for="brand"
                                                   class="col-sm-4 col-form-label text-md-right">{{ __('Brand') }}</label>

                                            <div class="col-md-6">
                                                <input id="brand" type="text"
                                                       class="form-control{{ $errors->has('brand') ? ' is-invalid' : '' }}"
                                                       name="brand" value="{{ old('brand') }}" placeholder="{{ __('Example: Chevrolet') }}" autofocus>

                                                @if ($errors && $errors->has('brand'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row">
                                            <label for="model"
                                                   class="col-md-4 col-form-label text-md-right">{{ __('Model') }}</label>

                                            <div class="col-md-6">
                                                <input id="model" type="text"
                                                       class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}"
                                                       name="model" value="{{ old('model') }}" placeholder="{{ __('Example: Corvette') }}">

                                                @if ($errors->has('model'))
                                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-8 offset-md-4">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Add car') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/autocomplete.js') }}"></script>
@endpush

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'BlaBlaCar') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>

    <!-- Fonts -->
    @stack('styles')
    <link rel="dns-prefetch" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css"
          integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style.css') }}" rel="stylesheet">
</head>
<body>
<div id="app">
    <nav class="navbar navbar-expand-md navbar-light navbar-laravel">
        <div class="container">
            <a class="navbar-brand" href="{{ url('/') }}">
                {{ config('app.name', 'BlaBlaCar') }}
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false"
                    aria-label="{{ __('Toggle navigation') }}">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <!-- Left Side Of Navbar -->
                <ul class="navbar-nav mr-auto">

                </ul>

                <!-- Right Side Of Navbar -->
                <ul class="navbar-nav ml-auto">
                    <!-- Authentication Links -->
                    @guest
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}"><i
                                        class="fa fa-sign-in-alt"></i> {{ __('Login') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('register') }}"><i
                                        class="fa fa-user-plus"></i> {{ __('Register') }}</a>
                        </li>
                    @else

                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('home') }}"><i
                                        class="fa fa-search"></i> {{ __('Find a ride') }}</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('travel.create') }}"><i
                                        class="fa fa-plus-circle"></i> {{ __('Offer a ride') }}</a>
                        </li>

                        <li class="nav-item dropdown">
                            <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button"
                               data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                {{ Auth::user()->first_name }} <span class="caret"></span>
                            </a>

                            <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                <a class="dropdown-item" href="{{ route('travel.offered') }}">
                                    <i class="fa fa-user"></i> <i
                                            class="fa fa-angle-double-right"></i> {{ __('Rides offered') }}
                                </a>
                                <a class="dropdown-item" href="{{ route('travel.booked') }}">
                                    <i class="fa fa-user"></i> <i
                                            class="fa fa-angle-double-left"></i> {{ __('Rides booked') }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('user.edit') }}">
                                    <i class="fa fa-user"></i> {{ __('Profile') }}
                                </a>
                                <div class="dropdown-divider"></div>
                                <a class="dropdown-item" href="{{ route('logout') }}"
                                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                    <i class="fa fa-sign-out-alt"></i> {{ __('Logout') }}
                                </a>

                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                      style="display: none;">
                                    @csrf
                                </form>
                            </div>
                        </li>
                    @endguest
                </ul>
            </div>
        </div>
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>

<!--footer starts from here-->
<footer class="footer">

    <div class="container">
        <ul class="foote_bottom_ul_amrc">
            <li><a href="{{ route('home') }}">{{ __('Find a ride') }}</a></li>
            <li><a href="{{ route('travel.create') }}">{{ __('Offer a ride') }}</a></li>
            <li><a href="{{ route('user.edit') }}">{{ __('Profile') }}</a></li>
            <li class="dropdown dropright">
                <a class="dropdown-toggle" href="#" role="button" id="dropdownMenuLink"
                   data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {{ __('Language:') }} {{ Config::get('app.locale') }}
                </a>
                <ul class="dropdown-menu">
                    @foreach (Config::get('app.locales') as $lang => $language)
                        <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}">{{ __($language) }}</a>
                    @endforeach
                </ul>
            </li>

        </ul>
        <!--foote_bottom_ul_amrc ends here-->
        <p class="text-center">Created by Anna Zinchenko</p>

    </div>

</footer>

@stack('scripts')

</body>
</html>

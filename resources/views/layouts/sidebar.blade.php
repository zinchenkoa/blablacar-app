<div class="col-md-4">
    <div class="list-group">
        <a class="list-group-item list-group-item-action disabled">{{ __('Profile') }}</a>
        <a href="/user/edit"
           class="list-group-item list-group-item-action {{ request()->is('user/*/edit') ? 'active' : '' }}">{{ __('Personal information') }}</a>
        <a href="/image"
           class="list-group-item list-group-item-action {{ request()->is('image') ? 'active' : '' }}">{{ __('Profile photo') }}</a>
        <a href="/user/car"
           class="list-group-item list-group-item-action {{ request()->is('user/car') ? 'active' : '' }}">{{ __('My car') }}</a>
    </div>
    <br>
    <div class="list-group">
        <a class="list-group-item list-group-item-action disabled">{{ __('Reviews') }}</a>
        <a href="/review/received"
           class="list-group-item list-group-item-action {{ request()->is('review/received') ? 'active' : '' }}">{{ __('Ratings you\'ve received') }}</a>
        <a href="/review/leaved"
           class="list-group-item list-group-item-action {{ request()->is('review/leaved') ? 'active' : '' }}">{{ __('Ratings you\'ve left') }}</a>
    </div>
    <br>
    <div class="list-group">
        <a class="list-group-item list-group-item-action disabled">{{ __('Account') }}</a>
        <a href="/user/change-password"
           class="list-group-item list-group-item-action {{ request()->is('user/change-password') ? 'active' : '' }}">{{ __('Password') }}</a>
        <a href="/user/remove"
           class="list-group-item list-group-item-action {{ request()->is('user/remove') ? 'active' : '' }}">{{ __('Delete my account') }}</a>
    </div>
</div>
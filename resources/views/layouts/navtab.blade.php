<div class="col-md-12">
    <ul class="nav nav-tabs nav-fill">
        <li class="nav-item">
            <a class="nav-link
                {{ request()->is('travel/offered') ||
                request()->is('travel/offered/history')
                ? 'active' : '' }}"
               href="{{ route('travel.offered') }}">{{ __('Rides offered') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link
                {{ request()->is('travel/booked') ||
                request()->is('travel/booked/history')
                ? 'active' : '' }}"
               href="{{ route('travel.booked') }}">{{ __('Rides booked') }}</a>
        </li>
        <li class="nav-item">
            <a class="nav-link
                {{ request()->is('user/edit') ||
                request()->is('image') ||
                request()->is('user/remove') ||
                request()->is('review/leaved') ||
                request()->is('review/received')||
                request()->is('user/change-password')||
                request()->is('user/car')
                ? 'active' : '' }}"
               href="/user/edit">{{ __('Profile') }}</a>
        </li>
    </ul>
</div>
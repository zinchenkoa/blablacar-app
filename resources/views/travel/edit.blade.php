@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        @if(Session::has('message'))
                            <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                        @endif

                        <form action="/travel/{{ $travel->id }}" method="post">
                            @csrf
                            @method('PUT')


                            <div class="form-group row">
                                <label for="start" class="col-sm-4 col-form-label text-md-right">From</label>

                                <div class="col-md-6">
                                    <input id="start" type="text"
                                           class="form-control" name="start"
                                           value="{{ old('start', $travel->start_city->name) }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="destination" class="col-md-4 col-form-label text-md-right">To</label>

                                <div class="col-md-6">
                                    <input id="destination" type="text"
                                           class="form-control"
                                           name="destination"
                                           value="{{ old('destination', $travel->destination_city->name) }}"
                                           readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">Date</label>

                                <div class="col-md-6">
                                    <input id="date" type="date" class="form-control"
                                           name="date" value="{{ old('date', $travel->date) }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">Price</label>

                                <div class="col-md-6">
                                    <input id="price" type="number" min="0"
                                           class="form-control" name="price"
                                           value="{{ old('price', $travel->price) }}" readonly>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="free_places"
                                       class="col-md-4 col-form-label text-md-right">Passengers</label>

                                <div class="col-md-6">
                                    <input id="free_places" type="number" min="0"
                                           class="form-control{{ $errors->has('free_places') ? ' is-invalid' : '' }}"
                                           name="free_places" value="{{ old('free_places', $travel->free_places) }}">

                                    @if ($errors->has('free_places'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('free_places') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="about" class="col-md-4 col-form-label text-md-right">About ride</label>

                                <div class="col-md-6">
                                 <textarea id="about" name="about" placeholder="Additional information"
                                           class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}">{{ old('about', $travel->about) }}</textarea>
                                    @if ($errors->has('about'))
                                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $errors->first('about') }}</strong>
                        </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Save
                                    </button>
                                </div>
                            </div>
                        </form>
                        <br>
                        <form action="/travel/{{ $travel->id }}" method="post">
                            @csrf
                            @method('DELETE')
                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-danger"
                                            onclick="return confirm('Are you sure you want to delete the ride?')">
                                        Delete ride
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

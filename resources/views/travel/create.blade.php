@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-body">

                        <form action="/travel" method="post">
                            @csrf

                            <div class="form-group row">
                                <label for="start" class="col-sm-4 col-form-label text-md-right">{{ __('From') }}</label>

                                <div class="col-md-6">
                                    <input id="start" type="text"
                                           class="form-control{{ $errors->has('start_id') ? ' is-invalid' : '' }}"
                                           name="start" value="{{ old('start') }}" autofocus>
                                    <input id="start_id" hidden="hidden" type="text" name="start_id">

                                    @if ($errors && $errors->has('start_id'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('start_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="destination" class="col-md-4 col-form-label text-md-right">{{ __('To') }}</label>

                                <div class="col-md-6">
                                    <input id="destination" type="text"
                                           class="form-control{{ $errors->has('destination_id') ? ' is-invalid' : '' }}"
                                           name="destination" value="{{ old('destination') }}">
                                    <input id="destination_id" hidden="hidden" type="text" name="destination_id">

                                    @if ($errors->has('destination_id'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('destination_id') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="date" class="col-md-4 col-form-label text-md-right">{{ __('Date') }}</label>

                                <div class="col-md-6">
                                    <input id="date" type="date" min="{{ $today }}"
                                           class="form-control{{ $errors->has('date') ? ' is-invalid' : '' }}"
                                           name="date" value="{{ old('date') }}">

                                    @if ($errors->has('date'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('date') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="time" class="col-md-4 col-form-label text-md-right">{{ __('Time') }}</label>

                                <div class="col-md-6">
                                    <input id="time" type="time"
                                           class="form-control{{ $errors->has('time') ? ' is-invalid' : '' }}"
                                           name="time" value="{{ old('time') }}">

                                    @if ($errors->has('time'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('time') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Price') }}</label>

                                <div class="col-md-6">
                                    <input id="price" type="number" min="0"
                                           class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}"
                                           name="price" value="{{ old('price') }}">

                                    @if ($errors->has('price'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="free_places"
                                       class="col-md-4 col-form-label text-md-right">{{ __('Passengers') }}</label>

                                <div class="col-md-6">
                                    <input id="free_places" type="number" min="1"
                                           class="form-control{{ $errors->has('free_places') ? ' is-invalid' : '' }}"
                                           name="free_places" value="{{ old('free_places') }}">

                                    @if ($errors->has('free_places'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('free_places') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="about" class="col-md-4 col-form-label text-md-right">{{ __('Information about trip') }}
                                    </label>

                                <div class="col-md-6">
                                    <textarea id="about" name="about" rows="5"
                                              placeholder="{{ __('Specify the exact place of embarkation and disembarkation.') }}"
                                              class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}">{{ old('about') }}</textarea>
                                    @if ($errors->has('about'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Add') }}
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/autocomplete.js') }}"></script>
@endpush

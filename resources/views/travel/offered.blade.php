@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')

                    <div class="card-body">

                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link active" href="/travel/offered">{{ __('Upcoming rides') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" href="/travel/offered/history">{{ __('Ride history') }}</a>
                            </li>
                        </ul>

                        @if(!$travels->isEmpty())
                            <br>
                            <h5>You have {{ $count = $travelsCount }} upcoming ride{{$travelsCount === 1 ? '' : 's'}}.</h5>

                            @foreach($travels as $travel)
                                <div class="row border single-travel">
                                    <div class="col-sm-5">
                                        <h4>{{ $travel->full_time }}</h4>
                                        {{ $travel->full_travel }}
                                        <br>
                                        @can('update', $travel)
                                            <a href="/travel/{{ $travel->id }}/edit"
                                               class="btn-warning btn show-btn">Edit ride</a>
                                        @endcan
                                    </div>
                                    <div class="col-sm-5">
                                        @if(!$travel->passengers->isEmpty())
                                            <p>Passengers in this ride:</p>
                                            @foreach($travel->passengers as $passenger)
                                               <p>
                                                   <a href="/user/{{ $passenger->id }}"
                                                      title="{{ $passenger->first_name }}">
                                                       <img src="{{ $passenger->profile_image }}"
                                                            class="profile-image-small" alt="Profile image">
                                                       {{ $passenger->first_name }}
                                                       {{ $passenger->last_name }}
                                                   </a>
                                               </p>
                                            @endforeach
                                        @else
                                            <p>No passengers in this ride so far.</p>
                                        @endif
                                    </div>
                                    <div class="col-sm-2">
                                        <h2>₴{{ $travel->price }}</h2>
                                        <span class="small">per passenger</span>
                                        <p><b>{{ $travel->free_places }}</b> available seats</p>
                                    </div>
                                </div>
                            @endforeach
                            {{ $travels->links() }}

                        @else
                            <br>
                            <p>You don't have any upcoming rides.</p>
                            <p>You can offer a new ride by clicking on:</p>
                            <a class="btn btn-success" href="{{ route('travel.create') }}/">Offer a ride</a>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

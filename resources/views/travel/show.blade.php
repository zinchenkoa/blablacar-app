@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">

                    <div class="card-body">

                        <div class="col-md-12">
                            <p class="small text-right">{{ __('Publication date:') }} {{ $travel->publication_date }} </p>
                        </div>
                        <div class="row cell">
                            <div class="col-sm-8 border">
                                <table class="table table-borderless">
                                    <tbody>
                                    <tr>
                                        <td>{{ __('Place of departure:') }}</td>
                                        <th><i class="fa fa-map-marker-alt"></i>
                                            {{ $travel->start_city->name }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{ __('Place of arrival:') }}</td>
                                        <th><i class="fa fa-map-marker-alt"></i>
                                            {{ $travel->destination_city->name }}</th>
                                    </tr>
                                    <tr>
                                        <td>{{ __('Date:') }}</td>
                                        <th><i class="fa fa-calendar-alt"></i>
                                            {{ $travel->full_time }}</th>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            @if(isset($travel->about))
                                                {{ __('Additional information:') }} {{ $travel->about }}
                                            @else
                                                {{ $travel->user->first_name }} {{ __('didn\'t provide additional
                                                information.') }}
                                            @endif
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            @if(!$travel->passengers->isEmpty())
                                                <p>{{ __('Passengers in this ride:') }}</p>
                                                @foreach($travel->passengers as $passenger)
                                                    <a href="/user/{{ $passenger->id }}"
                                                       title="{{ $passenger->first_name }}">
                                                        <img src="{{ $passenger->profile_image }}"
                                                             class="profile-image-small" alt="Profile image">
                                                    </a>
                                                @endforeach
                                                <br>
                                            @else
                                                <p>{{ __('No passengers in this ride.') }}</p>
                                            @endif
                                            <br>

                                            @if (Session::has('success'))
                                                <div class="alert alert-success">{{ Session::get('success') }}</div>
                                            @endif
                                            @if (Session::has('failure'))
                                                <div class="alert alert-danger">{{ Session::get('failure') }}</div>
                                            @endif

                                            @can('update', $travel)
                                                <a href="/travel/{{ $travel->id }}/edit"
                                                   class="btn-warning btn show-btn">{{ __('Edit ride') }}</a>
                                            @endcan

                                            @can('book', $travel)
                                                <div class="alert alert-info">
                                                    <strong>{{ __('Note: This booking will be confirmed automatically.') }}</strong>
                                                </div>
                                                <a href="/travel/{{ $travel->id }}/book" class="btn-success btn"
                                                   onclick="return confirm('{{ __("Do you want to book this ride?") }}')">{{ __('Book ride') }}</a>
                                            @endcan

                                            @can('cancelBooking', $travel)
                                                <form action="/travel/{{ $travel->id }}/cancel_booking" method="post">
                                                    @csrf
                                                    @method('DELETE')
                                                    <div class="form-group">
                                                        <button type="submit" class="btn btn-danger"
                                                                onclick="return confirm('{{ __("Do you want to cancel ride?") }}')">
                                                            {{ __('Cancel booking') }}
                                                        </button>
                                                    </div>
                                                </form>
                                            @endcan
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-sm-4">
                                <div class="row">
                                    <div class="col-md-6 text-md-center border">
                                        <h2 class="margin-top">₴{{ $travel->price }}</h2>
                                        <p class="small">{{ __('per passenger') }}</p>
                                    </div>
                                    <div class="col-md-6 text-md-center border">
                                        <h2 class="margin-top"><i
                                                    class="fas fa-car"></i> {{ $travel->available_places }}</h2>
                                        <p class="small">{{ __('available seats') }}</p>
                                    </div>
                                    <div class="col-md-12 border">
                                        <p class="margin-top"><b>{{ __('Driver') }}</b></p>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img src="{{ $travel->user->profile_image }}"
                                                     class="profile-image" alt="Profile image">
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="/user/{{ $travel->user->id }}">
                                                    <h4>{{ $travel->user->short_name }}</h4></a>
                                                @if(isset($travel->user->birth_year))
                                                    <p class="small small-cell">{{ __('age:') }} {{ $travel->user->age }} {{ __('y.o.') }}</p>
                                                @endif
                                                @if(isset($travel->avg_score))
                                                    <p class="small">{{ __('rating:') }} {{ $travel->avg_score }}/5 -
                                                        <a href="/user/{{ $travel->user->id }}">
                                                            {{ $count = count($travel->user->review) }}
                                                            {{ __('rating') }}
                                                        </a>
                                                    </p>
                                                @endif
                                            </div>
                                            <div class="col-md-12">
                                                <hr>
                                                @if(isset($travel->user->car))
                                                    <p><b>{{ __('Car') }}</b></p>
                                                    <p>{{ $travel->user->car->full_name }}</p>
                                                    <hr>
                                                @endif
                                                @if(isset($travel->user->review))
                                                    <p><b>{{ __('Reviews') }}</b>
                                                        <span class=float-right><a href="/user/{{ $travel->user->id }}">{{ __('Watch all reviews') }}</a></span>
                                                    </p>
                                                    @foreach($travel->user->review as $review)
                                                        <div class="row">
                                                            <div class="col-md-12 text-md-center">
                                                                <img src="{{ $review->owner->profile_image }}"
                                                                     alt="Profile image" class="profile-image-small">
                                                            </div>
                                                            <div class="col-md-12 text-md-center">
                                                                <p><b>{{ $review->literal_score }}</b></p>
                                                                <p><b>{{ __('From') }}
                                                                        <a href="/user/{{ $review->owner->id }}">
                                                                            {{ $review->owner->first_name }}
                                                                        </a>
                                                                    </b>: {{ $review->about }}
                                                                </p>
                                                                <p class="small">{{ $review->literal_created_at }}</p>
                                                            </div>
                                                        </div>
                                                        <hr>
                                                    @endforeach
                                                @else
                                                    <p>{{ __('No feedback for') }} {{ $travel->user->first_name }}</p>
                                                    <hr>
                                                @endif
                                                <p><b>{{ __('User\'s activity') }}</b></p>
                                                <p>{{ __('Offered trips:') }} {{ $countDriversTravels }}</p>
                                                <p>{{ __('Registered at:') }} {{ $travel->user->register_month }}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

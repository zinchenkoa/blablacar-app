@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">

                        <ul class="nav nav-pills">
                            <li class="nav-item">
                                <a class="nav-link" href="/travel/booked">{{ __('Current') }}</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link active" href="/travel/booked/history">{{ __('History') }}</a>
                            </li>
                        </ul>

                        @if(!$travels->isEmpty())
                            <br>
                            <h5>You booked {{ $count = $travelsCount }} ride{{$travelsCount === 1 ? '' : 's'}}</h5>

                            @foreach($travels as $travel)
                                <div class="row border single-travel">
                                    <div class="col-sm-4 border-right">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <img src="{{ $travel->user->profile_image }}"
                                                     class="profile-image" alt="Profile image">
                                            </div>
                                            <div class="col-sm-6">
                                                <a href="/user/{{ $travel->user->id }}">{{ $travel->user->short_name }}</a>
                                                @if(isset($travel->user->birth_year))
                                                    <p class="small">age: {{ $travel->user->age }} y.o.</p>
                                                @endif
                                                @if(isset($travel->user->car))
                                                    <p class="small">car: {{ $travel->user->car->full_name }}</p>
                                                @endif
                                            </div>
                                        </div>
                                        @if($travel->avg_score > 0)
                                            <p class="text-md-center"><i class="far fa-star"></i> rating: {{ $travel->avg_score }}/5 -
                                                <a href="/user/{{ $travel->user->id }}">
                                                    {{ $count = count($travel->user->review) }}
                                                    rating{{ $count > 1 ? 's' : '' }}
                                                </a>
                                            </p>
                                            @else
                                            <p class="text-md-center">No feedback</p>
                                        @endif
                                    </div>
                                    <div class="col-sm-6">
                                        <h4>{{ $travel->full_time }}</h4>
                                        {{ $travel->full_travel }}
                                        <br>
                                        <a class="btn btn-primary" href="/travel/{{ $travel->id }}">More info</a>
                                        @can('createReview', [\App\Models\Review::class, $travel->user, $travel])
                                            <a href="/review/create/{{ $travel->user->id }}/{{ $travel->id }}"
                                               class="btn btn-success">Leave a feedback</a>
                                        @endcan
                                    </div>
                                    <div class="col-sm-2">
                                        <h2>₴{{ $travel->price }}</h2>
                                        <span class="small">per passenger</span>
                                        <p><b>{{ $travel->free_places }}</b> available seats</p>
                                    </div>
                                </div>
                            @endforeach
                            {{ $travels->links() }}

                        @else
                            <br>
                            <p>No booked rides in your ride history.</p>
                        @endif

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

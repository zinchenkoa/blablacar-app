@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    @include('layouts.navtab')
                    <div class="card-body">
                        <div class="row">
                            @include('layouts.sidebar')
                            <div class="col-8">

                                @if(Session::has('message'))
                                    <p class="alert {{ Session::get('alert-class', 'alert-info') }}">{{ Session::get('message') }}</p>
                                @endif

                                @if(!$reviews->isEmpty())
                                    <h4>{{ count($reviews) }} feedback</h4>

                                    @foreach($reviews as $review)
                                        <hr>
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12 col-md-2 col-lg-2">
                                                <img src="{{ $review->user->profile_image }}"
                                                     alt="Profile image"
                                                     class="profile-image-small">
                                            </div>
                                            <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                                                <p><b>{{ $review->literal_score }}</b></p>
                                                <p><b>To
                                                        <a href="/user/{{ $review->user->id }}">
                                                            {{ $review->user->first_name }}
                                                        </a></b>: {{ $review->about }}
                                                </p>
                                                <p class="small">{{ $review->created_at }}</p>
                                            </div>
                                        </div>
                                    @endforeach
                                    {{ $reviews->links() }}
                                @else
                                    <p>You didn't leave any feedback</p>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

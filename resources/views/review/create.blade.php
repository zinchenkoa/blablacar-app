@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">Add trip</div>
                    <div class="card-body">

                        <h4>
                            Tell the community about your ride with {{ $user->first_name }}</h4>

                        <form action="/review/{{ $user->id }}/{{ $travel->id }}" method="post">
                            @csrf

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-md-right">{{ $user->first_name }}
                                    was:</label>

                                <div class="col-md-6">
                                    <label><input type="radio" id="type" name="type" value="1"> a driver during the ride</label>
                                    <br>
                                    <label><input type="radio" id="type" name="type" value="0"> a passenger during the
                                        ride</label>
                                </div>
                                @if ($errors && $errors->has('type'))
                                    <div class="offset-4 col-md-6">
                                    <span class="invalid-feedback" role="alert" style="display: inline">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                    </div>
                                @endif
                            </div>

                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label text-md-right">How was your
                                    experience?</label>

                                <div class="col-md-6">
                                    <input type="radio" name="score" value="5"> Outstanding
                                    <br>
                                    <input type="radio" name="score" value="4"> Excellent
                                    <br>
                                    <input type="radio" name="score" value="3"> Good
                                    <br>
                                    <input type="radio" name="score" value="2"> Poor
                                    <br>
                                    <input type="radio" name="score" value="1"> Very disappointing

                                </div>
                                @if ($errors && $errors->has('score'))
                                    <div class="offset-4 col-md-6">
                                    <span class="invalid-feedback" role="alert" style="display: inline">
                                        <strong>{{ $errors->first('score') }}</strong>
                                    </span>
                                    </div>
                                @endif

                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label text-md-right">
                                    Tell other members about your ride with this member, your feedback is useful to them
                                    (10-400 symbols):
                                </label>

                                <div class="col-md-6">
                                    <textarea name="about" rows="5"
                                              class="form-control{{ $errors->has('about') ? ' is-invalid' : '' }}">{{ old('about') }}</textarea>
                                    @if ($errors->has('about'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('about') }}</strong>
                                    </span>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group row mb-0">
                                <div class="col-md-8 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        Leave a feedback
                                    </button>
                                </div>
                            </div>
                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
@endpush

@push('scripts')
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <script type="text/javascript" src="{{ asset('js/autocomplete.js') }}"></script>
@endpush

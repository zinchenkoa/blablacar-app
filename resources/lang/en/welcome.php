<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'New trips every day.' => 'New trips every day.',
    'Today' => 'Today',
    'Yesterday' => 'Yesterday',
    'Last week' => 'Last week',
    'Last month' => 'Last month',
    'Make this your least expensive journey ever.' => 'Make this your least expensive journey ever.',
    'Login and find a ride' => 'Login and find a ride',
    'Go literally anywhere.' => 'Go literally anywhere.',
    'Smart' => 'Smart',
    'With access to millions of journeys, you can quickly find people nearby travelling your way.'
    => 'With access to millions of journeys, you can quickly find people nearby travelling your way.',
    'Simple' => 'Simple',
    'Select who you want to travel with.' => 'Select who you want to travel with.',
    'Seamless' => 'Seamless',
    'Get to your exact destination, without the hassle. Carpooling cuts out transfers, queues and the waiting around the station time.' => 'Get to your exact destination, without the hassle. Carpooling cuts out transfers, queues and the waiting around the station time.',
    'Where do you want to drive to?' => 'Where do you want to drive to?',
    'Let\'s make this your least expensive journey ever.' => 'Пусть эта поездка станет самой бюджетной в вашей жизни!',
    'No account? Register!' => 'No account? Register!',

];
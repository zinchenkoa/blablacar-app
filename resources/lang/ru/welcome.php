<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Login and find a ride' => 'Зарегистрироваться',
    'New trips every day.' => 'Каждый день новые поездки.',
    'Today' => 'Сегодня',
    'Yesterday' => 'Вчера',
    'Last week' => 'На прошлой неделе',
    'Last month' => 'В прошлом месяце',
    'Go literally anywhere.' => 'Куда угодно. Откуда угодно.',
    'Smart' => 'Просто',
    'With access to millions of journeys, you can quickly find people nearby travelling your way.'
    => 'Среди миллионов попутчиков вы легко найдете тех, кто рядом и кому с вами по пути.',
    'Simple' => 'Быстро',
    'Select who you want to travel with.' => 'Введите точный адрес, чтобы увидеть свою идеальную поездку. Выбирайте сами, с кем хотите отправиться в дорогу. И бронируйте!',
    'Seamless' => 'Без хлопот',
    'Get to your exact destination, without the hassle. Carpooling cuts out transfers, queues and the waiting around the station time.' => 'Добирайтесь до места назначения без пересадок. В поездках с попутчиками не надо беспокоиться об очередях и часах, проведенных в ожидании на станции.',
    'Where do you want to drive to?' => 'Куда вы планируете поехать?',
    'No account? Register!' => 'Нет аккаунта? Зарегистрируйтесь!',
    'Make this your least expensive journey ever.' => 'Пусть эта поездка станет самой бюджетной в вашей жизни!',
];
<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
*/

Auth::routes()
;
Route::get('/', 'WelcomeController@index');

Route::get('lang/{lang}', ['as' => 'lang.switch', 'uses' => 'LanguageController@switchLang']);

//CityController
Route::get('search', 'CityController@search');

//CarController
Route::get('searchBrand', 'CarController@searchBrand');
Route::get('searchModel', 'CarController@searchModel');
Route::get('user/car', 'CarController@index')->name('car.index');
Route::post('user/car', 'CarController@store')->name('car.store');
Route::delete('user/car', 'CarController@destroy')->name('car.destroy');

//TravelController
Route::get('travel/offered', 'TravelController@offered')->name('travel.offered');
Route::get('travel/offered/history', 'TravelController@offeredHistory')->name('travel.offered_history');
Route::get('travel/booked', 'TravelController@booked')->name('travel.booked');
Route::get('travel/booked/history', 'TravelController@bookedHistory')->name('travel.booked_history');
Route::get('travel/{travel}/book', 'TravelController@book')->name('travel.book');
Route::delete('travel/{travel}/cancel_booking', 'TravelController@cancelBooking');
Route::resource('travel', 'TravelController');

//UserImageController
Route::get('image', 'UserImageController@index')->name('image');
Route::post('image/upload', 'UserImageController@upload')->name('image.upload');
Route::delete('image/delete', 'UserImageController@delete')->name('image.delete');

//UpdatePasswordController
Route::get('user/change-password', 'UpdatePasswordController@index')->name('password.form');
Route::put('user/change-password', 'UpdatePasswordController@update')->name('password.update');

//UserController
Route::get('user/edit', 'UserController@edit')->name('user.edit');
Route::get('user/remove', 'UserController@remove')->name('user.remove');
Route::get('user/{user}', 'UserController@show')->name('user.show');
Route::put('user/{user}', 'UserController@update')->name('user.update');
Route::delete('user/{user}', 'UserController@destroy')->name('user.destroy');

//ReviewController
Route::get('review/received', 'ReviewController@received')->name('review.received');
Route::get('review/leaved', 'ReviewController@leaved')->name('review.leaved');
Route::get('review/create/{user}/{travel}', 'ReviewController@create')->name('review.create');
Route::post('review/{user}/{travel}', 'ReviewController@store')->name('review.store');

//HomeController
Route::get('home', 'HomeController@index')->name('home');
<?php

use Faker\Generator as Faker;
use App\Models\Car;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\User::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->unique()->safeEmail,
        'phone' => array_random([$faker->numberBetween(1000000000,1999999999), null]),
        'birth_year' => array_random([$faker->numberBetween(1960,2000), null]),
        'about' => $faker->realText($faker->numberBetween(10,200)),
        'car_id' => array_random([Car::all()->random()->id, null]),
        'image' => array_random([$faker->image('public/uploads/images/profile',200,200, 'people', false), null]),
        'password' => '$2y$10$TKh8H1.PfQx37YgCzwiKb.KjNyWgaHb9cbcoQgdIVFlYg7B77UdFm', // secret
        'remember_token' => str_random(10),
    ];
});

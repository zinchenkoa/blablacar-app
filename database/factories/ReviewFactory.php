<?php

use Faker\Generator as Faker;
use App\Models\User;
use App\Models\Travel;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\Review::class, function (Faker $faker) {
    return [
        'owner_id' => User::all()->random()->id,
        'user_id' => User::all()->random()->id,
        'travel_id' => Travel::all()->random()->id,
        'score' => $faker->numberBetween(1, 5),
        'type' =>$faker->numberBetween(0, 1),
        'about' => $faker->realText($faker->numberBetween(50, 100)),
    ];
});

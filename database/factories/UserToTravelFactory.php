<?php

use Faker\Generator as Faker;
use App\Models\Travel;
use App\Models\User;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(App\Models\UserToTravel::class, function (Faker $faker) {
    $travels = Travel::orderBy('id', 'desc')->take(20)->pluck('id')->toArray();
    return [
        'user_id' => User::all()->random()->id,
        'travel_id' => $faker->randomElement($travels),
    ];
});

<?php

use Faker\Generator as Faker;
use App\Models\City;
use App\Models\User;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(\App\Models\Travel::class, function (Faker $faker) {

    $date = Carbon::today();

    $start_id = $faker->numberBetween(4150, 4153);
    //$start_id = City::all()->random()->city_id,
    $destination_id = $faker->numberBetween(4150, 4153);
    if ($start_id === $destination_id) {
        $destination_id++;
    }

    return [
        'start_id' => $start_id,
        'destination_id' => $destination_id,
        'date'  => $date->addDays($faker->numberBetween(0, 1))->format('Y-m-d'),
        'time' => $faker->time(),
        'price' => $faker->numberBetween(50, 400),
        'about' => $faker->realText($faker->numberBetween(20, 200)),
        'free_places' => $faker->numberBetween(1, 3),
        'user_id' => User::all()->random()->id,
        'status' => 1,
    ];
});

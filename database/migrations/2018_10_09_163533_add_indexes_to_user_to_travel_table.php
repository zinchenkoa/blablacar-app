<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddIndexesToUserToTravelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_to_travel', function (Blueprint $table) {
            $table->index('user_id');
            $table->index('travel_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user_to_travel', function (Blueprint $table) {
            $table->dropIndex('user_to_travel_user_id_index');
            $table->dropIndex('user_to_travel_travel_id_index');
        });
    }
}

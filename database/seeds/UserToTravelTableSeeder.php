<?php

use Illuminate\Database\Seeder;

class UserToTravelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\UserToTravel::class, 30)->create();
    }
}

<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        Eloquent::unguard();
//
//        $path = base_path() . '/database/sql/car.sql';
//        DB::unprepared(file_get_contents($path));
//        $this->command->info('Car table seeded!');
//
//        $path = base_path() . '/database/sql/city.sql';
//        DB::unprepared(file_get_contents($path));
//        $this->command->info('City table seeded!');

        $this->call([
            UsersTableSeeder::class,
            TravelTableSeeder::class,
            UserToTravelTableSeeder::class,
            ReviewTableSeeder::class,
        ]);

    }
}

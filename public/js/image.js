$(document).ready(function(){

    $('#upload_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: '/image/upload',
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success:function(data)
            {
                $('#message').css('display', 'block');
                $('#message').html(data.message);
                $('#message').addClass(data.class_name);
                $("#uploaded_image").attr('src', data.uploaded_image);
            }
        })
    });

    $('#delete_form').on('submit', function(event){
        event.preventDefault();
        $.ajax({
            url: '/image/delete',
            method:"POST",
            data:new FormData(this),
            dataType:'JSON',
            contentType: false,
            cache: false,
            processData: false,
            success:function(data)
            {
                $('#message').css('display', 'block');
                $('#message').html(data.message);
                $('#message').addClass(data.class_name);
                $("#uploaded_image").attr('src', data.uploaded_image);
            }
        })
    });

});
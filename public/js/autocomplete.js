$(function () {
    $("#start").autocomplete({
        source: '/search',
        minLength: 2,
        select: function (event, ui) {
            $('#start').val(ui.item.label);
            $('#start_id').val(ui.item.value);
            return false;
        }
    });
    $("#destination").autocomplete({
        source: '/search',
        minLength: 2,
        select: function (event, ui) {
            $('#destination').val(ui.item.label);
            $('#destination_id').val(ui.item.value);
            return false;
        }
    });
    $("#brand").autocomplete({
        source: '/searchBrand',
        minLength: 2,
        select: function (event, ui) {
            $('#brand').val(ui.item.label);
            return false;
        }
    });
    $("#model").autocomplete({
        source: function(request, response) {
            $.getJSON("/searchModel", {
                "brand": $('#brand').val(),
                "model": $('#model').val()
            }, response);
        },
        minLength: 2,
        select: function (event, ui) {
            $('#model').val(ui.item.label);
            return false;
        }
    });
});
<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Class User
 *
 * @package App
 * @property integer $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $phone
 * @property integer $birth_year
 * @property string $about
 * @property string $car
 * @property string $image
 * @property integer $status
 * @property integer $admin
 * @property integer $password
 * @property integer $remember_token
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Travel[] $passenger
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Review[] $review
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\Travel[] $travel
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\UserToTravel[] $usersPassengers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereAdmin($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereBirthYear($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereFirstName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereLastName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User wherePhone($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereUpdatedAt($value)
 * @property int|null $car_id
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\User whereCarId($value)
 */
class User extends Authenticatable
{
    use Notifiable;

    const DEFAULT_PROFILE_IMAGE = '/images/default.jpg';
    const PROFILE_IMAGE_PATH = '/uploads/images/profile/';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name', 'last_name', 'email', 'password', 'phone', 'birth_year', 'about', 'image',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


    protected static function boot()
    {
        parent::boot();

        static::deleting(function (User $user) {
            $user->passenger()->detach();
            $user->usersPassengers()->delete();
            $user->review()->delete();
            $user->usersReviews()->delete();
            $user->travel()->delete();
            $user->deleteImage();
        });
    }

    public function review()
    {
        return $this->hasMany('App\Models\Review');
    }

    public function usersReviews()
    {
        return $this->belongsTo('App\Models\Review', 'id', 'owner_id');
    }

    public function travel()
    {
        return $this->hasMany('App\Models\Travel');
    }

    public function usersPassengers()
    {
        return $this->hasManyThrough('App\Models\UserToTravel', 'App\Models\Travel');
    }

    public function passenger()
    {
        return $this->belongsToMany('App\Models\Travel', 'user_to_travel');
    }

    public function car()
    {
        return $this->hasOne('App\Models\Car', 'id', 'car_id');
    }

    /**
     * Removes users image
     * return void
     */
    public function deleteImage(): void
    {
        $imagePath = public_path(). self::PROFILE_IMAGE_PATH . $this->image;
        if(isset($this->image) && file_exists($imagePath))
        {
            unlink($imagePath);
        }
    }

    /**
     * @return string
     */
    public function getProfileImageAttribute(): string
    {
        $path = self::PROFILE_IMAGE_PATH;
        $pathToProfileImage = public_path() . $path . $this->image;
        return !empty($this->image) && file_exists($pathToProfileImage) ?
            $path . $this->image : self::DEFAULT_PROFILE_IMAGE;
    }

    /**
     * @return string
     */
    public function getShortNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name{0};
    }

    /**
     * @return string
     */
    public function getFullNameAttribute(): string
    {
        return $this->first_name . ' ' . $this->last_name;
    }

    /**
     * @return int
     */
    public function getAgeAttribute()
    {
        return date('Y') - $this->birth_year;
    }

    /**
     * @return string
     */
    public function getRegisterMonthAttribute()
    {
        return date("F Y", strtotime($this->created_at));
    }

    /**
     * @return float
     */
    public function getAvgScoreAttribute(): float
    {
        return round($this->review->pluck('score')->avg(), 1);
    }

}
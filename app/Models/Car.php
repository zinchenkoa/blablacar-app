<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\Car
 *
 * @property int $id
 * @property int $year
 * @property string $brand
 * @property string $model
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereBrand($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereModel($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Car whereYear($value)
 * @mixin \Eloquent
 */
class Car extends Model
{
    const CARS_IN_SEARCH_LIST = 10;
    const SEARCH_BY_BRAND = 'brand';
    const SEARCH_BY_MODEL = 'model';

    /**
     * The table associated with the model.
     * @var string
     */
    protected $table = 'car';


    public function getFullNameAttribute()
    {
        return $this->brand.' '.$this->model;
    }

}

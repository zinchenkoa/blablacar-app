<?php

namespace App\Models;

use Illuminate\Support\Facades\Redis;

/**
 * Class Stats
 * @package App\Models
 */
class Stats
{
    /**
     * @var integer
     */
    private $travelsToday;

    /**
     * @var integer
     */
    private $travelsYesterday;

    /**
     * @var integer
     */
    private $travelsLastWeek;

    /**
     * @var integer
     */
    private $travelsLastMonth;

    public function __construct()
    {
        $this->calculate();
    }

    public function calculate()
    {
        $this->travelsToday = Redis::get('travelsToday');
        $this->travelsYesterday = Redis::get('travelsYesterday');
        $this->travelsLastWeek = Redis::get('travelsLastWeek');
        $this->travelsLastMonth = Redis::get('travelsLastMonth');
    }

    /**
     * @return int|null
     */
    public function getTravelsToday(): ?int
    {
        return $this->travelsToday;
    }

    /**
     * @return int|null
     */
    public function getTravelsYesterday(): ?int
    {
        return $this->travelsYesterday;
    }

    /**
     * @return int|null
     */
    public function getTravelsLastWeek(): ?int
    {
        return $this->travelsLastWeek;
    }

    /**
     * @return int|null
     */
    public function getTravelsLastMonth(): ?int
    {
        return $this->travelsLastMonth;
    }


}
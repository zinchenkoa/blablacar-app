<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\UserToTravel
 *
 * @property int $id
 * @property int $user_id
 * @property int $travel_id
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToTravel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToTravel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToTravel whereTravelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToTravel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\UserToTravel whereUserId($value)
 * @mixin \Eloquent
 */
class UserToTravel extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_to_travel';
}

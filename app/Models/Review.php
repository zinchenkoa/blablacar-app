<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Review
 *
 * @package App
 * @property int $id
 * @property int $owner_id
 * @property int $user_id
 * @property int $travel_id
 * @property int $type
 * @property string $score
 * @property string $about
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property-read \App\Models\User $owner
 * @property-read \App\Models\User $user
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereOwnerId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereScore($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereUserId($value)
 * @mixin \Eloquent
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereTravelId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Review whereType($value)
 */
class Review extends Model
{

    const SCORE = [
        '5' => 'Outstanding',
        '4' => 'Excellent',
        '3' => 'Good',
        '2' => 'Poor',
        '1' => 'Very disappointing',
    ];

    const REVIEWS_PER_PAGE = 5;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'review';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'score',
        'about',
        'type',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function owner()
    {
        return $this->belongsTo('App\Models\User', 'owner_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    /**
     * @return string
     */
    public function getLiteralScoreAttribute(): string
    {
        return array_key_exists($this->score, self::SCORE)
            ? self::SCORE[$this->score]
            : 'Unknown score';// const
    }

    /**
     * @return string
     */
    public function getLiteralCreatedAtAttribute(): string
    {
        return date("F jS - g:ia", strtotime($this->created_at));
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Travel
 *
 * @package App
 * @property integer $id
 * @property City $start_id
 * @property City $destination_id
 * @property string $date
 * @property string $time
 * @property integer $price
 * @property string $about
 * @property integer $free_places
 * @property User $user_id
 * @property integer $status
 * @property \Illuminate\Support\Carbon $created_at
 * @property \Illuminate\Support\Carbon $updated_at
 * @property-read \App\Models\City $destination_city
 * @property-read \App\Models\City $start_city
 * @property-read \App\Models\User|null $user
 * @mixin \Eloquent
 * @property-read \Illuminate\Database\Eloquent\Collection|\App\Models\User[] $passengers
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereAbout($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereDate($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereDestinationId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereFreePlaces($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel wherePrice($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereStartId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereStatus($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\App\Models\Travel whereUserId($value)
 */
class Travel extends Model
{

    const SINGLE_TRAVEL_REVIEWS = 2;
    const TRAVELS_PER_PAGE = 5;
    const TRAVELS_HISTORY = '<';
    const TRAVELS_CURRENT = '>=';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'travel';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    public $fillable = [
        'start_id',
        'destination_id',
        'date',
        'time',
        'price',
        'about',
        'free_places',
        'user_id',
    ];

    public function start_city()
    {
        return $this->belongsTo('App\Models\City', 'start_id', 'city_id');
    }

    public function destination_city()
    {
        return $this->belongsTo('App\Models\City', 'destination_id', 'city_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Models\User', 'user_id', 'id');
    }

    public function passengers()
    {
        return $this->belongsToMany('App\Models\User', 'user_to_travel');
    }

    /**
     * @return string
     */
    public function getFullTimeAttribute(): string
    {
        return date("F jS Y", strtotime($this->date)) . ' - ' . date('g:ia', strtotime($this->time));
    }

    /**
     * @return string
     */
    public function getFullTravelAttribute(): string
    {
        return $this->start_city->name . ' -> ' . $this->destination_city->name;
    }

    /**
     * @return string
     */
    public function getPublicationDateAttribute(): string
    {
        return date("F jS - g:ia", strtotime($this->created_at));
    }

    /**
     * @return int
     */
    public function getAvailablePlacesAttribute(): int
    {
        return $this->free_places;
    }

    /**
     * @return float
     */
    public function getAvgScoreAttribute(): float
    {
        return round($this->user->review->pluck('score')->avg(), 1);
    }

}
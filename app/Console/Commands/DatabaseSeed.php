<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use DatabaseSeeder;
use Illuminate\Console\Command;
use ReviewTableSeeder;
use TravelTableSeeder;
use UsersTableSeeder;
use UserToTravelTableSeeder;
/**
 * Class DatabaseSeed
 * Adds travels to database twice a day.
 *
 * @package App\Console\Commands
 */
class DatabaseSeed extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'database:seed';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command adds travels, users, reviews to database.';


    /**
     * @param DatabaseSeeder $databaseSeeder
     */
    public function handle(DatabaseSeeder $databaseSeeder): void
    {
        $databaseSeeder->call(TravelTableSeeder::class);
        $databaseSeeder->call(UsersTableSeeder::class);
        $databaseSeeder->call(UserToTravelTableSeeder::class);
        $databaseSeeder->call(ReviewTableSeeder::class);
        $message = Carbon::now() . ' Tables seeded: travel, users, users_to_travel, review';
        $this->info($message);
    }
}

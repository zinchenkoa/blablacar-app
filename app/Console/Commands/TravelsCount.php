<?php

namespace App\Console\Commands;

use App\Models\Travel;
use Carbon\Carbon;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Redis;

class TravelsCount extends Command
{
    const TODAY = 'today';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'travels:count {type?}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Count travels for today, yesterday, last week, month';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $type = $this->argument('type');
        $today = Carbon::today();
        $tomorrow = Carbon::tomorrow();
        $yesterday = Carbon::yesterday();
        $weekAgo = Carbon::today()->subWeek();
        $monthAgo = Carbon::today()->subMonth();

        if ($type === self::TODAY) {
            $travelsToday = Travel::whereBetween('created_at', [$today, $tomorrow])
                ->count();

            Redis::set('travelsToday', $travelsToday);

            $this->info(Carbon::now() . " Travels count: today - $travelsToday");

        } else {
            $travelsYesterday = Travel::whereBetween('created_at', [$yesterday, $today])->count();
            $travelsLastWeek = Travel::whereBetween('created_at', [$weekAgo, $today])->count();
            $travelsLastMonth = Travel::whereBetween('created_at', [$monthAgo, $today])->count();

            Redis::set('travelsYesterday', $travelsYesterday);
            Redis::set('travelsLastWeek', $travelsLastWeek);
            Redis::set('travelsLastMonth', $travelsLastMonth);

            $format = '%s Travels count: yesterday - %s, last week - %s, last month - %s';
            $message = sprintf($format, Carbon::now(), $travelsYesterday, $travelsLastWeek, $travelsLastMonth);
            $this->info($message);
        }
    }
}

<?php

namespace App\Helpers;

/**
 * @package App\Helpers
 */
class CarHelper
{

    /**
     * @param array $cars
     * @param string $attribute
     * @return array
     */
    public static function prepareList(array $cars, string $attribute): array
    {
        $searchResult = [];
        if ($cars) {
            foreach ($cars as $car) {
                $searchResult[] = [
                    'label' => $car[$attribute],
                ];
            }
        }
        return $searchResult;
    }
}
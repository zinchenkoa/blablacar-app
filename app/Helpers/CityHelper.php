<?php

namespace App\Helpers;

/**
 * Class CityHelper
 * @package App\Helpers
 */
class CityHelper
{

    /**
     * @param array $cities
     * @return array
     */
    public static function prepareList(array $cities): array
    {
        $searchResult = [];
        if ($cities) {
            foreach ($cities as $city) {
                $searchResult[] = [
                    'label' => $city['name'],
                    'value' => $city['city_id']
                ];
            }
        }
        return $searchResult;
    }
}
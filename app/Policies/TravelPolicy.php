<?php

namespace App\Policies;

use App\Services\TravelService;
use App\Models\User;
use App\Models\Travel;
use Illuminate\Auth\Access\HandlesAuthorization;

class TravelPolicy
{
    use HandlesAuthorization;

    /**
     * @var TravelService
     */
    protected $travelService;

    public function __construct(TravelService $travelService)
    {
        $this->travelService = $travelService;
    }

    /**
     * Determine whether the user can update the travel.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Travel $travel
     *
     * @return bool
     */
    public function update(User $user, Travel $travel)
    {
        return $user->id === $travel->user_id
            && $travel->date >= date('Y-m-d');
    }

    /**
     * Determine whether the user can delete the travel.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Travel $travel
     *
     * @return bool
     */
    public function delete(User $user, Travel $travel)
    {
        return $user->id === $travel->user_id
            && $travel->date >= date('Y-m-d');
    }

    /**
     * Determine whether the user can book the travel.
     *
     * @param \App\Models\User $user
     * @param \App\Models\Travel $travel
     *
     * @return bool
     */
    public function book(User $user, Travel $travel)
    {
        return $user->id !== $travel->user_id
            && $travel->date >= date('Y-m-d')
            && $travel->free_places > 0
            && !$this->travelService->isTravelBookedByUser($travel->id, $user->id);
    }

    /**
     * Determine whether the user can cancel the booking
     *
     * @param \App\Models\User $user
     * @param \App\Models\Travel $travel
     *
     * @return bool
     */
    public function cancelBooking(User $user, Travel $travel)
    {
        return $this->travelService->isTravelBookedByUser($travel->id, $user->id)
            && $travel->date >= date('Y-m-d');
    }
}

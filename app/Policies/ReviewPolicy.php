<?php

namespace App\Policies;

use App\Services\ReviewService;
use App\Services\UserService;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ReviewPolicy
{
    use HandlesAuthorization;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * @var ReviewService
     */
    protected $reviewService;

    /**
     * ReviewPolicy constructor.
     *
     * @param UserService   $userService
     * @param ReviewService $reviewService
     */
    public function __construct(UserService $userService, ReviewService $reviewService)
    {
        $this->userService = $userService;
        $this->reviewService = $reviewService;
    }

    /**
     * Determine whether the user can create review for another user.
     *
     * @param User $user
     * @param User $anotherUser
     * @param Travel $travel
     *
     * @return bool
     */
    public function createReview(User $user, User $anotherUser, Travel $travel)
    {
        return $travel->date < date('Y-m-d')
            && $this->userService->hasRelationUserDriverInTravel($user, $anotherUser, $travel)
            && !$this->reviewService->isReviewLeavedByUser($user, $anotherUser, $travel);
    }
}

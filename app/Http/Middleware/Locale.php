<?php

namespace App\Http\Middleware;

use Closure;
use App;
use Config;
use Illuminate\Support\Facades\Cookie;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Cookie::get('locale');
        if (!array_key_exists($locale, Config::get('app.locales'))) {
            $locale = Config::get('app.locale');
        }
        App::setLocale($locale);

        return $next($request);
    }
}

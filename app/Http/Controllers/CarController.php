<?php

namespace App\Http\Controllers;

use App\Helpers\CarHelper;
use App\Models\Car;
use App\Http\Requests\StoreCar;
use App\Services\CarService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class CarController extends Controller
{

    /**
     * @var CarService
     */
    private $carService;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    private $user;

    /**
     * @param CarService $carService
     */
    public function __construct(CarService $carService)
    {
        $this->middleware('auth');
        $this->middleware(
            function ($request, $next) {
                $this->user = auth()->user();
                return $next($request);
            }
        );

        $this->carService = $carService;
    }

    /**
     * Displays users car or form of adding car.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $car = $this->carService->getUsersCar($this->user);
        return view('car.index', ['car' => $car, 'user' => $this->user]);
    }

    /**
     * Add car to user.
     *
     * @param StoreCar $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(StoreCar $request)
    {
        if ($this->carService->saveUsersCar($request, $this->user)) {
            Session::flash('message', __('Your car was added!'));
        } else {
            Session::flash('message', __('Oops! Your car wasn\'t added! We don\'t have it in our database :('));
        }
        return redirect("/user/car");

    }

    /**
     * Remove car from user.

     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy()
    {
        if ($this->carService->destroyUsersCar($this->user)) {
            Session::flash('message', __('Your car was deleted!'));
        } else {
            Session::flash('message', __('Your car wasn\'t deleted!'));
        }
        return redirect("/user/car");
    }

    /**
     * Find car brand by name
     *
     * @param  Request $request
     * @return array
     */
    public function searchBrand(Request $request): array
    {
        $brand = $request->term;
        $cars = $this->carService->findBrandByName($brand);

        return CarHelper::prepareList($cars, Car::SEARCH_BY_BRAND);

    }

    /**
     * Find car model by name
     *
     * @param  Request $request
     * @return array
     */
    public function searchModel(Request $request): array
    {
        $cars = $this->carService->findModelByNameInBrand($request->model, $request->brand);
        return CarHelper::prepareList($cars, Car::SEARCH_BY_MODEL);
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Services\UserService;
use Illuminate\Http\Request;
use Validator;

class UserImageController extends Controller
{

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    private $user;

    /**
     * @var UserService;
     */
    private $userService;

    /**
     * UserImageController constructor.
     */
    public function __construct(UserService $userService)
    {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });
        $this->userService = $userService;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('userImage.upload')->with('user', $this->user);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function upload(Request $request)
    {
        $validation = Validator::make(
            $request->all(),
            [
                'image' => 'required|image|mimes:jpeg,jpg|max:1024'
            ]
        );

        if ($validation->passes()) {
            $image = $request->file('image');

            if ($this->userService->setUser($this->user)->saveImage($image)) {
                return response()->json(
                    [
                        'message' => 'Image Uploaded Successfully',
                        'uploaded_image' => User::PROFILE_IMAGE_PATH . $this->user->image,
                        'class_name' => 'alert-success'
                    ]
                );
            }

        }
        return response()->json(
            [
                'message' => $validation->errors()->all(),
                'class_name' => 'alert-danger'
            ]
        );
    }


    public function delete()
    {
        if ($this->userService->setUser($this->user)->deleteImage()) {
            return response()->json(
                [
                    'message' => 'Image Deleted Successfully',
                    'uploaded_image' => User::DEFAULT_PROFILE_IMAGE,
                    'class_name' => 'alert-success'
                ]
            );
        }

        return response()->json(
            [
                'message' => 'You can\'t delete image',
                'class_name' => 'alert-danger'
            ]
        );
    }

}

<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdatePassword;
use App\Services\UserService;
use Illuminate\Support\Facades\Session;

class UpdatePasswordController extends Controller
{

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    private $user;

    /**
     * @var UserService
     */
    private $userService;

    /*
     * Ensure the user is signed in to access this page
     */
    public function __construct(UserService $userService)
    {

        $this->middleware('auth');
        $this->middleware(
            function ($request, $next) {
                $this->user = auth()->user();
                return $next($request);
            }
        );

        $this->userService = $userService;

    }

    /**
     * Show the form to change the user password.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('user.change-password');
    }

    /**
     * Update the password for the user.
     *
     * @param UpdatePassword $request
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(UpdatePassword $request)
    {

        if ($this->userService->setUser($this->user)->updateUsersPassword($request)) {
            Session::flash('success', __('Your password has been changed.'));
        } else {
            Session::flash('failure', __('Your password has not been changed.'));
        }
        return back();
    }
}

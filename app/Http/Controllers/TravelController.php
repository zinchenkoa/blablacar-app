<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTravel;
use App\Http\Requests\UpdateTravel;
use App\Services\TravelService;
use App\Models\Travel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class TravelController extends Controller
{
    /**
     * @var TravelService
     */
    protected $travelService;

    public function __construct(TravelService $travelService)
    {
        $this->middleware('auth');
        $this->travelService = $travelService;
    }

    /**
     * Display a listing of travels with $request options.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $travels = $this->travelService->findAll($request);
        return view(
            'travel.index',
            [
                'travels' => $travels,
                'travelsCount' => $travels->total()
            ]
        );
    }

    /**
     * Display a list of user's offered travels.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function offered(Request $request)
    {
        $travels = $this->travelService->findOffered(
            $request->user()->id,
            Travel::TRAVELS_CURRENT
        );
        return view(
            'travel.offered',
            [
                'travels' => $travels,
                'travelsCount' => $travels->total()
            ]
        );
    }

    /**
     * Display a history of user's old offered travels.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function offeredHistory(Request $request)
    {
        $travels = $this->travelService->findOffered(
            $request->user()->id,
            Travel::TRAVELS_HISTORY
        );
        return view(
            'travel.offered_history',
            [
                'travels' => $travels,
                'user' => $request->user()
            ]
        );
    }

    /**
     * Display a list of user's booked travels.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function booked(Request $request)
    {
        $travels = $this->travelService->findBooked(
            $request->user()->id,
            Travel::TRAVELS_CURRENT
        );

        return view(
            'travel.booked', [
                'travels' => $travels,
                'travelsCount' => $travels->total()
            ]
        );
    }

    /**
     * Display a history of user's old booked travels.
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function bookedHistory(Request $request)
    {
        $travels = $this->travelService->findBooked(
            $request->user()->id,
            Travel::TRAVELS_HISTORY
        );
        return view(
            'travel.booked_history',
            [
                'travels' => $travels,
                'travelsCount' => $travels->total()
            ]
        );
    }

    /**
     * Store a booking application in storage.
     *
     * @param Request $request
     * @param Travel $travel
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function book(Request $request, Travel $travel)
    {
        $this->authorize('book', $travel);
        if ($this->travelService->setTravel($travel)->bookTravel($request)) {
            Session::flash('success', __('You booked this travel!'));
        } else {
            Session::flash('failure', __('This travel wasn\'t booked!'));
        }
        return back();
    }

    /**
     * Cancel a reservation.
     *
     * @param Request $request
     * @param Travel $travel
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function cancelBooking(Request $request, Travel $travel)
    {
        $this->authorize('cancelBooking', $travel);
        if ($this->travelService->setTravel($travel)->cancelBooking($request)) {
            Session::flash('success', __('Your booking was cancelled!'));
        } else {
            Session::flash('failure', __('Your booking wasn\'t cancelled!'));
        }
        return back();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('travel.create', ['today' => date('Y-m-d')]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CreateTravel $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(CreateTravel $request)
    {
        $travelId = $this->travelService->storeTravel($request);
        Session::flash('message', __('Your trip was added!'));
        return redirect("travel/$travelId");

    }

    /**
     * Display the specified resource.
     *
     * @param Travel $travel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(Travel $travel)
    {
        $travel = $this->travelService->setTravel($travel)->findOne();
        $countDriversTravels = $this->travelService->countOffered($travel->user->id);

        return view('travel.show', [
            'travel' => $travel,
            'countDriversTravels' => $countDriversTravels,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param Travel $travel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function edit(Travel $travel)
    {
        $this->authorize('update', $travel);

        return view('travel.edit', ['travel' => $travel])
            ->with(['start_city', 'destination_city']);
    }

    /**
     * Update the specified resource in storage.
     * @param UpdateTravel $request
     * @param Travel $travel
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function update(UpdateTravel $request, Travel $travel)
    {
        $this->authorize('update', $travel);
        $travelId = $this->travelService->setTravel($travel)->updateTravel($request);

        if ($travelId) {
            Session::flash('success', __('Your travel was updated!'));
        } else {
            Session::flash('failure', __('Your travel wasn\'t updated!'));
        }
        return redirect("travel/$travelId");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param Travel $travel
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function destroy(Travel $travel)
    {
        $this->authorize('delete', $travel);

        $result = $this->travelService->setTravel($travel)->destroyTravel();
        $result ? Session::flash('message', __('Your ride was deleted!'))
            : Session::flash('message', __('You can\'t delete this travel!'));
        return redirect("travel/offered");
    }
}

<?php

namespace App\Http\Controllers;

use App\Helpers\CityHelper;
use App\Services\CityService;
use Illuminate\Http\Request;

class CityController extends Controller
{

    /**
     * @var CityService
     */
    private $cityService;

    /**
     * @param CityService $cityService
     */
    public function __construct(CityService $cityService)
    {
        $this->middleware('auth');
        $this->cityService = $cityService;
    }

    /**
     * Find city by name
     *
     * @param  Request $request
     * @return array
     */
    public function search(Request $request): array
    {
        $name = $request->term;
        $cities = $this->cityService->findCitiesByName($name);
        return CityHelper::prepareList($cities);
    }
}

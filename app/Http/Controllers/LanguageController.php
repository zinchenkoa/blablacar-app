<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Cookie;

class LanguageController extends Controller
{
    public function switchLang($lang)
    {
        $cookie = null;

        if (array_key_exists($lang, config('app.locales'))) {
            $cookie = Cookie::forever('locale', $lang);
        }

        if ($cookie) {
            return redirect()->back()->withCookie($cookie);
        }

        return back();
    }
}
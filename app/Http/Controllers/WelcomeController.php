<?php

namespace App\Http\Controllers;

use App\Models\Stats;

class WelcomeController extends Controller
{

    /**
     * Show the application dashboard.
     *
     * @param Stats $stats
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Stats $stats)
    {
        return view('welcome', ['stats' => $stats]);
    }
}

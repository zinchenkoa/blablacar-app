<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateReview;
use App\Services\ReviewService;
use App\Services\UserService;
use App\Models\Travel;
use App\Models\Review;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

/**
 * Class ReviewController
 * @package App\Http\Controllers
 */
class ReviewController extends Controller
{
    /**
     * @var ReviewService
     */
    protected $reviewService;

    /**
     * @var UserService
     */
    protected $userService;

    /**
     * ReviewController constructor.
     *
     * @param ReviewService $reviewService
     *
     * @param UserService $userService
     */
    public function __construct(ReviewService $reviewService, UserService $userService)
    {
        $this->middleware('auth');
        $this->reviewService = $reviewService;
        $this->userService = $userService;
    }

    /**
     * Display received reviews
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function received(Request $request)
    {
        $userId = $request->user()->id;
        $reviews = $this->reviewService->findReceived($userId);
        return view(
            'review.received', [
                'user' => $request->user(),
                'reviews' => $reviews,
            ]
        );
    }

    /**
     * Display leaved reviews
     *
     * @param Request $request
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function leaved(Request $request)
    {
        $reviews = $this->reviewService->findLeaved($request->user()->id);
        return view('review.leaved', ['reviews' => $reviews]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param User $user
     * @param Travel $travel
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function create(User $user, Travel $travel)
    {

        $this->authorize(
            'createReview',
            [
                Review::class,
                $user,
                $travel
            ]
        );

        return view(
            'review.create',
            [
                'user' => $user,
                'travel' => $travel
            ]
        );
    }

    /**
     * Store a newly created resource in storage
     *
     * @param CreateReview $request
     * @param User $user
     * @param Travel $travel
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function store(CreateReview $request, User $user, Travel $travel)
    {

        $this->authorize(
            'createReview',
            [
                Review::class,
                $user,
                $travel
            ]
        );

        if ($this->reviewService->store($request, $user, $travel)) {
            Session::flash('success', __('Your review was added!'));
        } else {
            Session::flash('failure', __('Your review wasn\'t added!'));
        }
        return redirect("review/leaved");
    }

}

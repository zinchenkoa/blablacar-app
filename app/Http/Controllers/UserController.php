<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Services\ReviewService;
use App\Services\TravelService;
use App\Services\UserService;
use App\Models\User;
use Illuminate\Support\Facades\Session;

class UserController extends Controller
{
    /**
     * @var UserService
     */
    private $userService;

    /**
     * @var ReviewService
     */
    private $reviewService;

    /**
     * @var TravelService
     */
    private $travelService;

    /**
     * @var \Illuminate\Contracts\Auth\Authenticatable
     */
    private $user;

    /**
     * @param UserService $userService
     * @param ReviewService $reviewService
     * @param TravelService $travelService
     */
    public function __construct(
        UserService $userService,
        ReviewService $reviewService,
        TravelService $travelService
    ) {
        $this->middleware('auth');
        $this->middleware(function ($request, $next) {
            $this->user = auth()->user();
            return $next($request);
        });

        $this->userService = $userService;
        $this->reviewService = $reviewService;
        $this->travelService = $travelService;
    }

    /**
     * Display the specified User.
     *
     * @param User $user
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show(User $user)
    {
        $reviews = $this->reviewService->findReceived($user->id);
        $countUserTravels = $this->travelService->countOffered($user->id);
        return view('user.show', [
                'user' => $user,
                'reviews' => $reviews ?? null,
                'countUserTravels' => $countUserTravels,
                'reviewsCount' => $reviews->total(),
            ]
        );
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit()
    {
        return view('user.edit')->with('user', $this->user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateUser $request
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(UpdateUser $request)
    {
        $userId = $this->userService->updateUser($request, $this->user);
        Session::flash('message', __('Your profile was updated!'));
        return redirect("user/edit");
    }

    /**
     * Show the page for removing User.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function remove()
    {
        return view('user.remove')->with('user', $this->user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy()
    {
        $this->userService->destroyUser($this->user);
        return redirect("home");
    }

}

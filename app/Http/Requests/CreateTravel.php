<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateTravel extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {

        return true;

    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_id' => 'required|integer',
            'destination_id' => 'required|integer|different:start_id',
            'date' => 'required|date|after:yesterday',
            'time' => 'required|date_format:H:i',
            'price' => 'required|integer|min:0|max:20000',
            'free_places' => 'required|integer|min:1|max:6',
            'about' => 'nullable|min:3|max:1000',
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_id.required' => 'Start city is required. Choose city from dropdown list.',
            'destination_id.required'  => 'Destination city is required. Choose city from dropdown list.',
        ];
    }
}

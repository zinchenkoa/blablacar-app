<?php

namespace App\Services;

use App\Models\Car;
use App\Http\Requests\StoreCar;
use App\Models\User;

class CarService
{
    /**
     * @var Car
     */
    private $car;

    public function __construct(Car $car)
    {
        $this->car = $car;
    }

    /**
     * @param string $name
     * @return array
     */
    public function findBrandByName(string $name): array
    {
        return $this->car
            ->select('brand')
            ->where('brand', 'like', $name . '%')
            ->limit(Car::CARS_IN_SEARCH_LIST)
            ->groupBy('brand')
            ->get()
            ->toArray();
    }

    /**
     * @param string $model
     * @param string $brand
     * @return array
     */
    public function findModelByNameInBrand(string $model, ?string $brand): array
    {
        return $this->car
            ->select('model')
            ->where('brand', '=', $brand)
            ->where('model', 'like', $model . '%')
            ->limit(Car::CARS_IN_SEARCH_LIST)
            ->groupBy('model')
            ->get()
            ->toArray();
    }

    /**
     * @param User $user
     * @return Car|null
     */
    public function getUsersCar(User $user): ?Car
    {
        return $this->car
            ->select('car.*')
            ->join('users', 'car.id', '=', 'users.car_id')
            ->where('users.id', $user->id)
            ->first();
    }

    /**
     * @param StoreCar $request
     * @param User $user
     * @return bool
     */
    public function saveUsersCar(StoreCar $request, User $user): bool
    {
        $data = $request->validated();
        $carId = $this->getCarId($data);
        if($carId) {
            return $user
                ->where('id', $user->id)
                ->update(['car_id' => $carId]);
            }
        return false;
    }

    /**
     * @param int $userId
     * @return bool
     */
    public function destroyUsersCar(User $user): bool
    {
         return $user
             ->where('id', $user->id)
             ->update(['car_id' => null]);
    }

    /**
     * @param $data
     * @return int|null
     */
    private function getCarId($data): ?int
    {
        return $this->car
            ->where('brand', $data['brand'])
            ->where('model', $data['model'])
            ->value('id');
    }

}
<?php

namespace App\Services;

use App\Models\City;

class CityService
{
    /**
     * @var City
     */
    private $city;

    public function __construct(City $city)
    {
        $this->city = $city;
    }


    /**
     * @param string $name
     * @return array
     */
    public function findCitiesByName(string $name): array
    {
        return City::where('name', 'like', $name . '%')
            ->limit(City::CITIES_IN_SEARCH_LIST)
            ->get()
            ->toArray();
    }

}
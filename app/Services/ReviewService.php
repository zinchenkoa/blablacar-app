<?php

namespace App\Services;

use App\Http\Requests\CreateReview;
use App\Models\Review;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;

class ReviewService
{
    /**
     * @var Review
     */
    private $review;

    public function __construct(Review $review)
    {
        $this->review = $review;
    }

    /**
     * @param $userId
     * @return LengthAwarePaginator
     */
    public function findReceived($userId): LengthAwarePaginator
    {
        return $this->review
            ->where('user_id', $userId)
            ->with(['owner'])
            ->orderBy('review.created_at', 'DESC')
            ->paginate(Review::REVIEWS_PER_PAGE);
    }

    /**
     * @param $userId
     * @return LengthAwarePaginator
     */
    public function findLeaved($userId): LengthAwarePaginator
    {
        return $this->review
            ->where('owner_id', $userId)
            ->with(['user'])
            ->orderBy('created_at', 'DESC')
            ->paginate(Review::REVIEWS_PER_PAGE);
    }

    /**
     * @param CreateReview $request
     * @param User $anotherUser
     * @param Travel $travel
     * @return bool
     */
    public function store(CreateReview $request, User $anotherUser, Travel $travel): bool
    {
        $data = $request->validated();
        $this->review->fill($data);
        $this->review->owner_id = $request->user()->id;
        $this->review->user_id = $anotherUser->id;
        $this->review->travel_id = $travel->id;
        return $this->review->save();
    }

    /**
     * @param User   $user
     * @param User   $anotherUser
     * @param Travel $travel
     *
     * @return bool
     */
    public function isReviewLeavedByUser(User $user, User $anotherUser, Travel $travel): bool
    {
        return (bool)$this->review
            ->where('owner_id', $user->id)
            ->where('user_id', $anotherUser->id)
            ->where('travel_id', '=', $travel->id)
            ->exists();
    }
}
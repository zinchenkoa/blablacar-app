<?php

namespace App\Services;

use App\Http\Requests\UpdatePassword;
use App\Http\Requests\UpdateUser;
use App\Models\Travel;
use App\Models\User;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Hash;

class UserService
{
    /**
     * @var User
     */
    private $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * @param Travel $travel
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @param UpdateUser $request
     *
     * @return int
     */
    public function updateUser(UpdateUser $request, User $user): int
    {
        $data = $request->validated();
        $user->fill($data);
        $user->save();
        return $user->id;
    }

    /**
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function destroyUser(User $user): bool
    {
        return (bool)$user->delete();
    }

    /**
     * @param User $user
     * @param User $anotherUser
     * @param Travel $travel
     * @return bool
     */
    public function hasRelationUserDriverInTravel(User $user, User $anotherUser, Travel $travel): bool
    {
        return (bool)$user
            ->join('user_to_travel', 'users.id', '=', 'user_to_travel.user_id')
            ->join('travel', 'user_to_travel.travel_id', '=', 'travel.id')
            ->where(function ($query) use ($user, $anotherUser, $travel) {
                $query->where('user_to_travel.user_id', $user->id)
                    ->where('travel.user_id', $anotherUser->id)
                    ->where('travel.id', '=', $travel->id);

            })
            ->orWhere(function ($query) use ($user, $anotherUser, $travel) {
                $query->where('travel.user_id', $user->id)
                    ->where('user_to_travel.user_id', $anotherUser->id)
                    ->where('travel.id', '=', $travel->id);
            })
            ->exists();
    }

    /**
     * @param UpdatePassword $request
     * @return bool
     */
    public function updateUsersPassword(UpdatePassword $request): bool
    {
        $data = $request->validated();
        $hashedPassword = $this->user->password;

        if (Hash::check($data['old'], $hashedPassword)) {
            $this->user->fill(
                [
                    'password' => Hash::make($data['password'])
                ]
            );
            return $this->user->save();
        }
        return false;
    }

    /**
     * @param UploadedFile $image
     * @return bool
     */
    public function saveImage(UploadedFile $image): bool
    {
        if ($this->user->image) {
            $this->deleteImage();
        }
        $this->user->image = $image->hashName();
        $path = public_path(User::PROFILE_IMAGE_PATH);
        if ($image->move($path, $this->user->image)) {
            return $this->user->save();
        }
        return false;
    }

    /**
     * @return bool
     */
    public function deleteImage(): bool
    {
        $imagePath = public_path() . User::PROFILE_IMAGE_PATH . $this->user->image;
        if (file_exists($imagePath)) {
            $this->user->update(['image' => null]);
            return unlink($imagePath);
        }
        return false;
    }
}
<?php

namespace App\Services;

use App\Http\Requests\CreateTravel;
use App\Http\Requests\UpdateTravel;
use App\Models\Travel;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class TravelService
{
    /**
     * @var Travel
     */
    private $travel;

    public function __construct(Travel $travel)
    {
        $this->travel = $travel;
    }

    /**
     * @param Travel $travel
     * @return self
     */
    public function setTravel(Travel $travel): self
    {
        $this->travel = $travel;
        return $this;
    }

    /**
     * @param CreateTravel $request
     * @return int
     */
    public function storeTravel(CreateTravel $request): int
    {
        $data = $request->validated();
        $this->travel->fill($data);
        $this->travel->user_id = $request->user()->id;
        $this->travel->save();
        return $this->travel->id;
    }

    /**
     * @param CreateTravel $request
     * @return int
     */
    public function updateTravel(UpdateTravel $request): int
    {
        $data = $request->validated();
        $this->travel->fill($data);
        $this->travel->save();
        return $this->travel->id;
    }

    /**
     * @return bool
     * @throws \Exception
     */
    public function destroyTravel(): bool
    {
        if ($this->travel->date >= date('Y-m-d')) {
            return (bool)$this->travel->delete();
        }
        return false;
    }

    /**
     * @param Request $request
     * @return int
     */
    public function bookTravel(Request $request): int
    {
        $this->travel->passengers()->save($request->user());
        $this->travel->decrement('free_places');
        $this->travel->save();
        return $this->travel->id;
    }

    /**
     * @param Request $request
     * @return int
     */
    public function cancelBooking(Request $request): int
    {

        $this->travel->passengers()->detach($request->user());
        $this->travel->increment('free_places');
        $this->travel->save();
        return $this->travel->id;
    }

    /**
     * @return Travel
     */
    public function findOne(): Travel
    {
        return $this->travel
            ->whereId($this->travel->id)
            ->with(['start_city', 'destination_city', 'passengers', 'user.car'])
            ->with(['user.review' => function ($q) {
                $q->take(Travel::SINGLE_TRAVEL_REVIEWS);
            }])
            ->firstOrFail();
    }

    /**
     * @param Request $request
     * @return LengthAwarePaginator
     */
    public function findAll(Request $request): LengthAwarePaginator
    {
        $data = $request->query->all();
        $this->travel->fill($data);
        $travels = $this->travel
            ->where('start_id', $this->travel->start_id)
            ->where('destination_id', $this->travel->destination_id)
            ->where('date', $this->travel->date)
            ->where('free_places', '>', 0)
            ->with(['start_city', 'destination_city', 'user.review'])
            ->orderBy('date')
            ->orderBy('time');
        if ($this->travel->date === Carbon::now()->toDateString()) {
            $travels = $travels->where('time', '>', Carbon::now()->toTimeString());
        }
        $travels = $travels->paginate(Travel::TRAVELS_PER_PAGE)
            ->appends([
                'start_id' => $this->travel->start_id,
                'destination_id' => $this->travel->destination_id,
                'date' => $this->travel->date
            ]);
        return $travels;
    }

    /**
     * @param int $userId
     * @param string $sign
     * @return LengthAwarePaginator
     */
    public function findOffered(int $userId, string $sign): LengthAwarePaginator
    {
        $travels = $this->travel
            ->with(['start_city', 'destination_city', 'user', 'passengers'])
            ->where('user_id', $userId)
            ->where('date', $sign, Carbon::now('Europe/Kiev')->toDateString())
            ->orderBy('date')
            ->orderBy('time')
            ->paginate(Travel::TRAVELS_PER_PAGE);
        return $travels;
    }

    /**
     * @param int $userId
     * @return int
     */
    public function countOffered(int $userId): int
    {
        $travels = $this->travel
            ->where('user_id', $userId)
            ->count();
        return $travels;
    }

    /**
     * @param int $userId
     * @param string $sign
     * @return LengthAwarePaginator
     */
    public function findBooked(int $userId, string $sign): LengthAwarePaginator
    {
        $bookedTravels = $this->travel
            ->select('travel.*')
            ->join('user_to_travel', 'travel.id', '=', 'user_to_travel.travel_id')
            ->join('users', 'users.id', '=', 'user_to_travel.user_id')
            ->with(['start_city', 'destination_city', 'user.car', 'user.review', 'passengers'])
            ->where('users.id', $userId)
            ->where('date', $sign, Carbon::now('Europe/Kiev')->toDateString())
            ->orderBy('date')
            ->orderBy('time')
            ->paginate(Travel::TRAVELS_PER_PAGE);
        return $bookedTravels;
    }

    /**
     * @param int $travelId
     * @param int $userId
     * @return bool
     */
    public function isTravelBookedByUser(int $travelId, int $userId): bool
    {
        return (bool)$this->travel
            ->join('user_to_travel', 'travel.id', '=', 'user_to_travel.travel_id')
            ->join('users', 'users.id', '=', 'user_to_travel.user_id')
            ->where('users.id', $userId)
            ->where('travel.id', $travelId)
            ->exists();
    }

    /**
     * @param int $travelId
     * @return bool
     */
    public function isSeatAvailable(int $travelId): bool
    {
        $seatsAvailable = $this->travel
            ->where('id', $travelId)
            ->select('free_places')
            ->first();
        return $seatsAvailable->free_places > 0;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection
     */
    public function getPassengers(): Collection
    {
        return $this->travel
            ->passengers()
            ->get();
    }
}
